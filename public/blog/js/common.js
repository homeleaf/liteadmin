$(function () {
    $(document).on('click','a[target!="_blank"]',function (ev) {

        if(history.pushState){
            ev.preventDefault();
            var url = $(ev.target).attr('href');
            if (url === location.pathname){
                return;
            }
            $.get(url).then(function (html) {
                var html = $(html).find('#pjax-container').html();
                var title = $(html).find('title').text();
                $('#pjax-container').html(html);
                window.history.pushState(200, title, url);
            })
        }
    })
})