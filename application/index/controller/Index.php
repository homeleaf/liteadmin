<?php
/**
 * 河北纽扣网络科技
 * http://www.newcou.com/
 * User: 范杰
 * Date: 2018/9/26
 * Time: 11:20
 */

namespace app\index\controller;

use think\Controller;

class Index extends Controller
{
    public function index()
    {
        return $this->fetch();
    }
}