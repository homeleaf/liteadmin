<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/9/29
 * Time: 23:44
 */

namespace app\blog\widget;

use app\common\model\Category;
use think\Controller;

class Blog extends Controller
{
    public function nav()
    {
        $category = Category::all(['state'=>1,'is_deleted'=>0]);
        $this->assign('category',$category);
        return $this->fetch('widget/nav');
    }
}