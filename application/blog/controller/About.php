<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/9/28
 * Time: 23:40
 */

namespace app\blog\controller;

use think\Controller;

/**
 * 关于控制器
 * Class About
 * @package app\blog\controller
 */
class About extends Controller
{
    public function index()
    {
        return $this->fetch();
    }
}