<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/9/29
 * Time: 21:38
 */

namespace app\common\model;

use think\Model;

class Article extends Model
{
    protected $table = 'content_article';

    public function tags()
    {
        return $this->belongsToMany(Tags::class,TagsMap::class,'tag_id','article_id');
    }
}