<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/9/30
 * Time: 0:11
 */

namespace app\common\model;

use think\Model;

class Link extends Model
{
    protected $table = "content_link";
}