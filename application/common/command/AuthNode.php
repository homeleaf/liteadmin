<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/4/22 0022
 * Time: 下午 20:15
 */

namespace app\common\command;

use jay\Surport\Node;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\Db;

/**
 * 更新节点数据
 * Class AuthNode
 * @package app\common\command
 */
class AuthNode extends Command
{
    protected function configure(){
        $this->setName("authnode")->setDescription("update auth node");
    }
    
    protected function execute(Input $input, Output $output){
        app('node')->reload();
        $output->writeln("Auth node data has been updated!");
    }
}