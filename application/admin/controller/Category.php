<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/5/28
 * Time: 22:00
 */

namespace app\admin\controller;

use app\common\controller\BasicAdmin;
use think\Db;
use think\facade\Request;

/**
 * @title 分类管理
 * Class Category
 * @package app\admin\controller
 */
class Category extends BasicAdmin
{
    protected $table = "ContentCategory";

    /**
     * @title 列表页
     * @return array|mixed|\PDOStatement|string|\think\Collection
     */
    public function index()
    {
        $db = Db::name($this->table)
            ->alias('c1')
            ->join('__CONTENT_CATEGORY__ c2','c2.id = c1.pid','LEFT')
            ->field('c1.*,c2.title as ptitle')
            ->where('c1.is_deleted','=',0)
            ->order('id asc');
        return $this->_list($db);
    }

    /**
     * @title 添加
     * @return mixed
     */
    public function add()
    {
        return $this->_form('', 'form');
    }

    /**
     * @title 编辑
     * @return mixed
     */
    public function edit()
    {
        return $this->_form('', 'form');
    }

    /**
     * 表单前置
     * @param $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    protected function _form_before(&$data)
    {
        if (Request::isGet()){
            $list = Db::name($this->table)->select();
            $tree = app('tree')->array2tree($list);

            $func = function (&$tree) use ($data, &$func){
                $idnow = empty($data)?0:$data['id'];
                foreach ($tree as $key => &$item){
                    if ($item['id'] == $idnow) {
                        unset($tree[$key]);
                        return true;
                    }
                    if (isset($item['child'])){
                        if ($func($item['child'])){
                            return true;
                        }
                    }
                }
            };
            $func($tree);

            $cates = app('tree')->tree2list($tree);

            $this->assign('cates',$cates);
        }else{
            $content = Request::post('content','','strval');
            $config = \HTMLPurifier_Config::createDefault();
            $config->set('HTML.SafeIframe',true);
            $config->set('URI.SafeIframeRegexp','%^http://player.youku.com%');
            $config->set('Attr.AllowedFrameTargets',[
                'height' => true,
                'width' => true,
                'src' => true,
                'frameborder' => true,
                'allowfullscreen' => true
            ]);
            $purfier = new \HTMLPurifier($config);
            $data['content'] = $purfier->purify($content);;
        }
    }

    /**
     * 表单后置
     * @param $data
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    protected function _form_after(&$data)
    {
        $ppath = Db::name($this->table)->where('id','=',$data['pid'])->value('path')?:0;

        $children = Db::name($this->table)->whereLike('path', $data['path'].',%')->column('path','id');
        Db::name($this->table)->where('id','=',$data['id'])->update([
            'path'=>$ppath.','.$data['id']
        ]);

        foreach ($children as $id =>$path){
            Db::name($this->table)->where('id','=',$id)->update([
                'path'=>str_replace($data['path'],$ppath.','.$data['id'], $path)
            ]);
        }
    }

    /**
     * @title 删除
     * @throws PDOException
     * @throws \think\Exception
     */
    public function del()
    {
        $ids = Request::get('ids');
        $son = Db::name($this->table)
            ->whereIn('pid',$ids)
            ->where('is_deleted',0)
            ->count();
        if ((int)$son !== 0){
            $this->error("存在子类，不能删除！");
        }
        $this->_del($ids);
    }

    /**
     * @title 禁用/启用操作
     * @throws \think\Exception
     */
    public function change()
    {
        $id = Request::get('id');
        $state = Request::get('state');
        $this->_change($id, ['state' => $state]);
    }
}