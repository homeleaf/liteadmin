<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/4/11 0011
 * Time: 下午 22:09
 */

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\facade\Request;
use think\facade\Session;


/**
 * @title 登录控制器
 * Class Login
 * @package app\admin\controller
 */
class Login extends Controller
{
    /**
     * @title 登录操作
     * @auth 0
     * @return mixed|\think\response\Redirect
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function login()
    {
        if (session('admin.id')){
            $this->success("请不要重复登陆！",'admin/index/index');
        }
        if (Request::isGet()){
            return $this->fetch();
        }else{
            $username = Request::post('username',false);
            $password = Request::post('password',false);
            $verify = Request::post('verify',false);

            (!$username || !$password || !$verify) && $this->error("缺少参数！");

            if (strlen($username) < 5 || strlen($username) > 20)
                $this->error("用户名在5-20字符之间！");
            if (strlen($password) < 5 || strlen($password) > 20)
                $this->error("用户名在5-20字符之间！");

            if (!captcha_check($verify)){
                $this->error("验证码错误！");
            }

            $user = Db::name('SystemAdmin')->where('username','=',$username)->find();
//            dump(password_hash('123123',PASSWORD_DEFAULT));
//            exit();
            if (empty($user))
                $this->error("用户名输入不正确！");
            if (!password_verify($password, $user['password']))
                $this->error("密码输入不正确！");
            if ($user['state'] !==1)
                $this->error("该用户已被禁用！");

            $options = [
                'cost'=>config('password.cost')
            ];
            if (password_needs_rehash($user['password'], PASSWORD_DEFAULT, $options)) {
                // 如果是这样，则创建新散列，替换旧散列
                $newpassword = password_hash($password, PASSWORD_DEFAULT, $options);
                $update_data['password']=$newpassword;
            }
            Session::set('admin',$user);
            // 最后登录时间
            $update_data['last_login'] = Request::time();
            Db::name('SystemAdmin')->where('username','=',$username)->update($update_data);

            $this->success("登录成功！",'admin/index/index');
        }
    }

    /**
     * @title 退出登录
     * @auth 1
     */
    public function logout()
    {
        Session::set('admin',null);
        return $this->redirect('admin/login/login');
    }
}