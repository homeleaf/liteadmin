<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/4/1 0001
 * Time: 下午 23:18
 */

namespace app\admin\controller;

use app\common\controller\BasicAdmin;

/**
 * @title 权限节点
 * Class Node
 * @package app\admin\controller
 */
class Node extends BasicAdmin
{
    protected $table = "SystemAuthNode";

    /**
     * @title 列表页
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $list = app('node')->getNodesData();
        $this->assign("list", $list);
        return $this->fetch();
    }
    
    /**
     * @title 刷新权限数据
     */
    public function clear()
    {
        app('node')->reload();
        $this->success('操作成功', '');
    }
}