<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/6/15
 * Time: 16:27
 */

namespace app\admin\controller;

use app\common\controller\BasicAdmin;
use think\Db;
use think\exception\PDOException;
use think\facade\Request;

/**
 * @title 站点配置信息
 * Class Config
 * @package app\admin\controller
 */
class Config extends BasicAdmin
{
    protected $table = "SiteConfig";

    /**
     * @title 设置站点
     * @return mixed
     */
    public function edit()
    {
        $db = Db::name($this->table);
        if (Request::isGet()){
            $fields = $db->select();
            $this->assign('fields',$fields);
            return $this->fetch('form');
        }else{
            $fields = Request::post();
            foreach ($fields as $name => $value){
                try{
                    $db->where('name','=', $name)->update(['value'=>$value]);
                }catch (PDOException $e){
                    $this->error($e->getMessage(),'');
                }
                $db->removeOption();
            }
            $this->success('数据操作成功！','');
        }
    }
}