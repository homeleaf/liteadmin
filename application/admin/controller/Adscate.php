<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/6/7
 * Time: 16:10
 */
namespace app\admin\controller;

use app\common\controller\BasicAdmin;
use think\Db;
use think\facade\Request;

/**
 * @title 广告分类管理
 * Class Adscate
 * @package app\admin\controller
 */
class Adscate extends BasicAdmin
{
    protected $table = "ContentAdscate";

    /**
     * @title 列表页
     * @return array|mixed|\PDOStatement|string|\think\Collection
     */
    public function index()
    {
        $db = Db::name($this->table)
            ->where('is_deleted','=',0);

        $search = Request::get();
        // 精准查询
        foreach (['state'] as $field){
            if (isset($search[$field]) && $search[$field] !== ''){
                $db->where($field,'=', $search[$field]);
            }
        }
        // 模糊查询
        foreach (['title'] as $field){
            if (isset($search[$field]) && $search[$field] !== ''){
                $db->whereLike($field, "%{$search[$field]}%");
            }
        }

        return $this->_list($db, true, $search);
    }

    /**
     * @title 添加
     * @return mixed
     */
    public function add()
    {
        return $this->_form('', 'form');
    }

    /**
     * @title 编辑
     * @return mixed
     */
    public function edit()
    {
        return $this->_form('', 'form');
    }

    /**
     * @title 删除
     * @throws PDOException
     * @throws \think\Exception
     */
    public function del()
    {
        $ids = Request::get('ids');
        $this->_del($ids);
    }

    /**
     * @title 禁用/启用操作
     * @throws \think\Exception
     */
    public function change()
    {
        $id = Request::get('id');
        $state = Request::get('state');
        $this->_change($id, ['state' => $state]);
    }
}