<?php
/**
 * https://gitee.com/Mao02
 * http://blog.dazhetu.cn/
 * jay_fun 410136330@qq.com
 * Date: 2018/4/8 0008
 * Time: 上午 0:04
 */

namespace app\admin\controller;

use app\common\controller\BasicAdmin;
use jay\Surport\Tree;
use think\Db;
use think\facade\Request;

/**
 * @title 用户角色管理
 * Class Role
 * @package app\admin\controller
 */
class Role extends BasicAdmin
{
    
    protected $table = "SystemRole";
    
    /**
     * @title 列表页
     * @return array|mixed|\PDOStatement|string|\think\Collection|\think\Paginator
     */
    public function index()
    {
        $db = Db::name($this->table);
        return $this->_list($db);
    }
    
    /**
     * @title 添加
     * @return mixed
     */
    public function add()
    {
        return $this->_form('', 'form');
    }
    
    /**
     * @title 编辑
     * @return mixed
     */
    public function edit()
    {
        return $this->_form('', 'form');
    }
    
    /**
     * @title 授权
     * @return mixed
     */
    public function access()
    {
        return $this->_form('');
    }

    /**
     * 授权前置
     * @param $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function _access_form_before(&$data)
    {
        if (Request::isGet()) {
            $data['node'] = explode(',', $data['access_list']);
            $nodes = app('node')->getNodesData();
            $this->assign('nodes', $nodes);
        } else {
            $data['access_list'] = empty($data['node'])?'':implode(',', $data['node']);
            unset($data['node']);
        }
    }
    
    /**
     * @title 删除
     */
    public function del()
    {
        $ids = Request::get('ids');
        $this->_del($ids);
    }
}