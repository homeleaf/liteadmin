# liteadmin

#### 项目介绍

后台开发框架的轻量实现，后端基于tp5.1；后台 UI 使用了基于 layui 的 [xadmin 2.0](http://x.xuebingsi.com/)

#### 软件架构

我尽可能提供一个简洁纯净的后台开发框架，只提供一些基础大众的功能，以及一些示例功能，
并没有内置过多的功能组件，如果项目有需要可以自行composer安装相关组件。

本项目创建之初受到了thinkAdmin的启发，如果需要一个功能强大且完备的后台框架，可以参考使用[thinkAdmin](https://demo.thinkadmin.top/admin.html#/admin/index/main.html?spm=m-7)。

本项目致力于满足于小型项目的后台快速开发。

#### 安装教程

1. 代码 clone 到本地，执行composer update
2. liteadmin.sql 导入数据库
3. 执行优化命令

#### 使用说明

以下不算是文档，只是想到哪记到哪，等到完全开发完成后会编制详尽的文档，多谢关注。

1. 权限

    权限系统使用注解实现，@title为控制器或方法命名，@auth为方法指定鉴权级别。

    注解 auth 0：不验证，1：验证登录，2：验证授权 不填默认是2
    
    在权限节点页面点击刷新权限节点或者命令行运行 `php think authnode`来将代码的权限结构同步到数据库
    
2. 按钮

    后台使用button按钮做data-*组件可以防止连续点击，a标签则可能触发多次

3. 权限节点

    节点探测时有两处例外
    
    1 `app\common\service\Node::getFileNodes()` 中 使用的 app.admin_module 配置项。指定哪些是要进行权限节点扫描的module，
    
    2 `Node::reload()` 中 ignore 数组，上面 `getFileNodes` 方法返回的列表中，指定要滤掉不要的具体节点，在这里进行例外配置。
    
    重写父类的方法和下划线开头的方法以及非public修饰的方法会被忽略；
    
4. 优化命令

```$xslt
php think optimize:autoload
```
指令执行成功后，会在rumtime目录下面生成classmap.php文件，生成的类库映射文件会扫描系统目录和应用目录的类库。

```
php think optimize:config
```
默认生成应用的配置缓存文件，调用后会在runtime目录下面生成init.php文件，生成配置缓存文件后，应用目录下面的config.phpcommon.php以及tags.php不会被加载，被runtime/init.php取代。

```$xslt
php think optimize:schema
```
会自动生成当前数据库配置文件中定义的数据表字段缓存，也可以指定数据库生成字段缓存（必须有用户权限），例如，下面指定生成demo数据库下面的所有数据表的字段缓存信息。

```$xslt
php think optimize:route
```
执行后，会在runtime目录下面生成route.php文件。

5. 数据库备份

命令行执行 `php think databackup` 可以将sql文件导出到根目录的liteadmin.sql文件中 

6. 上传组件

上传功能已经组件化，具体使用请参照文章缩略图上传，之后会抽时间细化文档

7. 富文本编辑器

目前集成的是[wangEditor](http://www.wangeditor.com/)，具体请参照文章发表。

8. markdown编辑器

已经集成了[editor.md](http://pandao.github.io/editor.md/examples/index.html);

在文章管理中可以灵活使用以上两个编辑器，通过配置文件 `app.editor` 来配置 "html"或"markdown"来切换。

9. orm

BasicAdmin中定义的共有方法都是用的think\Db来实现的，具体业务中的独立逻辑也可以自行使用thinkPHP的模型进行数据库操作。

#### 已发现问题：

1 数据库抛出异常 Field * doesn't have a default value

解决方法 在配置文件中 /etc/mysql/my.cnf 中找到:
     sql-model=STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION，
     把STRICT_TRANS_TABLES去掉即可
2 abort方法中断请求并返回状态码 在调试模式是会抛出异常，使用异常页面模版渲染，正常显示404  403页面需要关闭调试模式。

     

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)