<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;
Route::rule('admin/login','admin/login/login','get|post');
Route::group('admin', function () {
    // 退出
    Route::rule('logout','admin/login/logout','get');
    // 管理员
    Route::group('admin',function (){
        Route::rule('/index','admin/admin/index','get');
        Route::rule('/add','admin/admin/add','get|post');
        Route::rule('/change','admin/admin/change','get');
        Route::rule('/del','admin/admin/del','get');
        Route::rule('/edit','admin/admin/edit','get|post');
        Route::rule('/password','admin/admin/password','get|post');
        Route::rule('/role','admin/admin/role','get|post');
    });
    // 文章
    Route::group('article',function (){
        Route::rule('/index','admin/article/index','get');
        Route::rule('/add','admin/article/add','get|post');
        Route::rule('/change','admin/article/change','get');
        Route::rule('/del','admin/article/del','get');
        Route::rule('/edit','admin/article/edit','get|post');
    });
    // 分类
    Route::group('category',function (){
        Route::rule('/index','admin/category/index','get');
        Route::rule('/add','admin/category/add','get|post');
        Route::rule('/change','admin/category/change','get');
        Route::rule('/del','admin/category/del','get');
        Route::rule('/edit','admin/category/edit','get|post');
    });
    // 首页
    Route::group('index',function (){
        Route::rule('/','admin/index/index','get');
        Route::rule('/welcome','admin/index/welcome','get');
        Route::rule('/edit','admin/index/edit','get|post');
        Route::rule('/password','admin/index/password','get|post');
    });
    // 友情链接
    Route::group('link',function (){
        Route::rule('/index','admin/link/index','get');
        Route::rule('/add','admin/link/add','get|post');
        Route::rule('/change','admin/link/change','get');
        Route::rule('/del','admin/link/del','get');
        Route::rule('/edit','admin/link/edit','get|post');
    });
    // 菜单
    Route::group('menu',function (){
        Route::rule('/index','admin/menu/index','get');
        Route::rule('/add','admin/menu/add','get|post');
        Route::rule('/del','admin/menu/del','get');
        Route::rule('/edit','admin/menu/edit','get|post');
    });
    // 角色
    Route::group('role',function (){
        Route::rule('/access','admin/role/access','get|post');
        Route::rule('/index','admin/role/index','get');
        Route::rule('/add','admin/role/add','get|post');
        Route::rule('/del','admin/role/del','get');
        Route::rule('/edit','admin/role/edit','get|post');
    });
    // 权限节点
    Route::group('node',function (){
        Route::rule('/index','admin/node/index','get');
        Route::rule('/clear','admin/node/clear','get');
    });
    // 文件上传
    Route::group('upload',function (){
        Route::rule('/file','admin/upload/file','post');
        Route::rule('/checkFile','admin/upload/checkFile','get');
        Route::rule('/ueditor','admin/upload/ueditor','post');
        Route::rule('/wangeditor','admin/upload/wangeditor','post');
    });
    // 站点
    Route::group('site',function (){
        Route::rule('/index','admin/site/index','get');
        Route::rule('/add','admin/site/add','get|post');
        Route::rule('/change','admin/site/change','get');
        Route::rule('/del','admin/site/del','get');
        Route::rule('/edit','admin/site/edit','get|post');
    });
    // 广告分类
    Route::group('adscate',function (){
        Route::rule('/index','admin/adscate/index','get');
        Route::rule('/add','admin/adscate/add','get|post');
        Route::rule('/change','admin/adscate/change','get');
        Route::rule('/del','admin/adscate/del','get');
        Route::rule('/edit','admin/adscate/edit','get|post');
    });
    // 广告
    Route::group('ads',function (){
        Route::rule('/index','admin/ads/index','get');
        Route::rule('/add','admin/ads/add','get|post');
        Route::rule('/change','admin/ads/change','get');
        Route::rule('/del','admin/ads/del','get');
        Route::rule('/edit','admin/ads/edit','get|post');
    });
    // 网站设置
    Route::group('config',function (){
        Route::rule('/edit','admin/config/edit','get|post');
    });
    // 标签设置
    Route::group('tags',function (){
        Route::rule('/index','admin/tags/index','get');
        Route::rule('/del','admin/tags/del','get');
    });
    // 路由强制
    Route::miss(function(\think\Request $req, \think\Response $res){
        return $res->content("开发中的地址")->code(404);
    });
})->middleware(\app\http\middleware\AccessCheck::class);

Route::rule('/','index/index/index','get');
Route::rule('index','index/index/index','get');

